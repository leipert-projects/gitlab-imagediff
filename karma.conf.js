module.exports = function(config) {
  config.set({
    browsers: ['ChromeHeadlessCustom'],
    customLaunchers: {
      ChromeHeadlessCustom: {
        base: 'ChromeHeadless',
        displayName: 'Chrome',
        flags: [
          // chrome cannot run in sandboxed mode inside a docker container unless it is run with
          // escalated kernel privileges (e.g. docker run --cap-add=CAP_SYS_ADMIN)
          '--no-sandbox',
        ],
      },
    },
    frameworks: ['browserify', 'jasmine'],
    files: [
      'spec/*.js',
      {
        pattern: 'spec/images/*',
        watched: false,
        included: false,
        served: true,
        nocache: false,
      },
    ],
    preprocessors: {
      'spec/*.js': ['browserify'],
    },
    reporters: ['progress'],
    proxies: {
      '/images/': '/base/spec/images/',
    },
  });
};
